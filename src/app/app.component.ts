import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MyProject';
  list:any = [];
  constructor(private http: HttpClient) {
    this.go();
   }

  go(){
    var headers = new HttpHeaders();
    headers = headers
      .append("Content-Type", "application/json")
      .append('Access-Control-Allow-Origin', '*');
    this.http.get("https://localhost:5001/api/WeatherForecast",{headers: headers}).subscribe(res=>
    {
      console.log(res)
      this.list = res;
    })
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppcontainerRoutingModule } from './appcontainer-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AppcontainerRoutingModule
  ]
})
export class AppcontainerModule { }

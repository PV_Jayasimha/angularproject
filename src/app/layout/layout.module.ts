import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { SideheaderComponent } from './sideheader/sideheader.component';
import { MainheaderComponent } from './mainheader/mainheader.component';
import { AppcontainerComponent } from './appcontainer/appcontainer.component';


@NgModule({
  declarations: [LayoutComponent, SideheaderComponent, MainheaderComponent, AppcontainerComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule
  ]
})
export class LayoutModule { }

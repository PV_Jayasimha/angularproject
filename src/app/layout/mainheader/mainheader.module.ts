import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainheaderRoutingModule } from './mainheader-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MainheaderRoutingModule
  ]
})
export class MainheaderModule { }

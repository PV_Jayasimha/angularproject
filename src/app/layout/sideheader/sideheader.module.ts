import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SideheaderRoutingModule } from './sideheader-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SideheaderRoutingModule
  ]
})
export class SideheaderModule { }
